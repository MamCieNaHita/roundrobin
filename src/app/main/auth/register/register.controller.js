(function ()
{
    'use strict';

    angular
        .module('app.auth.register')
        .controller('RegisterController', RegisterController);

    /** @ngInject */
    function RegisterController(AuthService, $mdDialog, $timeout, $state)
    {
        // Data
        var vm=this;
        // Methods
        vm.register=register;
        //////////
        function register(){
          AuthService.register(vm.form.username, vm.form.email, vm.form.password).then(function(response){
            if (response.success===false) {
                $mdDialog.show({
                  template: '<div style="width: 300px; height: 50px; display: flex; align-items: center; justify-content: center; background-color: #039BE5; color: white; font-weight: bold; font-family: sans;">'+response.error+'</div>'
                });
                $timeout(function(){$mdDialog.hide()}, 3000);
            }else{
              $mdDialog.show({
                template: '<div style="width: 300px; height: 50px; display: flex; align-items: center; justify-content: center; background-color: #039BE5; color: white; font-weight: bold; font-family: sans;">Successfully created user</div>'
              });
              $timeout(function(){
                $mdDialog.hide()
                $state.go('app.auth_login');
              }, 3000);
            }
          });
        }
    }
})();
