(function ()
{
    'use strict';

    angular
        .module('app.sample')
        .controller('SampleController', SampleController);

    /** @ngInject */
    function SampleController($scope, SampleData, $http, msApi)
    {
        var vm = this;
        vm.num=2;
        // Data
        vm.helloText = SampleData.data.helloText;
        //vm.data = this.query();
        //console.log(this.query());
        //console.log(SampleData);
        //console.log(vm);
        // Methods
       /* $scope.myGet = function(id) {
            if(id === undefined || id === null || id === NaN)
            {
                id = '';
            }
        $http.get('http://localhost:5000/project/'+ id).then(function(response) {
                vm.mydataunf = response.data;
                vm.mydata = '';
                for(var i=0; i<vm.mydataunf.length; i++)
                {
                    vm.mydata+= 'ID: ' + vm.mydataunf[i].m._id + '\n';
                    vm.mydata+= 'TITLE: ' + vm.mydataunf[i].m.properties.title + '\n';
                    vm.mydata+= 'RELEASED: ' + vm.mydataunf[i].m.properties.released + '\n\n';

                }
                console.log(response.data);
            })
    };*/
        $scope.myTest = function() {
            console.log("dupa");
        }

        $scope.myPost = function(title, year) {
            var data = { title: title, year: year };
            $http.post('http://localhost:5000/movie', data);
            console.log(data);
        }
        $scope.myDelete = function(id) {
            $http.delete('http://localhost:5000/movie/' + id);
        }
        $scope.myUpdate = function(id, title, year) {
            var data = { title: title, year: year};
            $http.put('http://localhost:5000/movie/' + id, data);
        }
        $scope.myGetList = function() {
            msApi.request('movie@query', {method:'GET'},
            function(response)
            {
                vm.mydata = response;
            })
        }
        $scope.myNewGet = function(id) {
            msApi.request('movieid@get', {id: id},
                function(response)
                {
                    vm.mydata = response;
                })
        }
        $scope.myNewPost = function(name, identifier, description) {
               msApi.request('project@save', {name: name, identifier: identifier, description: description})
        }
        $scope.myNewDelete = function(id) {
            msApi.request('movie@update', {title: title, year:year})
        }
        vm.mydata='';
        console.log(vm.mydata);
        //////////
        vm.widgets = [{ x:0, y:0, width:1, height:4, input: 'maly' }, { x:0, y:0, width:3, height:1, input: 'duzy' }];

    vm.options = {
       // cellHeight: 200,
        verticalMargin: 10,
        float: true,
        disableResize: true
    };

    vm.addWidget = function() {
        var newWidget = { x:0, y:0, width:1, height:1, input: 'nowy' };
        vm.widgets.push(newWidget);
        //console.log(vm);
    };

    vm.removeWidget = function(w) {
        var index = vm.widgets.indexOf(w);
        vm.widgets.splice(index, 1);
    };

    vm.onChange = function(event, items) {
        //$log.log("onChange event: "+event+" items:"+items);
    };

    vm.onDragStart = function(event, ui) {
      //  $log.log("onDragStart event: "+event+" ui:"+ui);
    };

    vm.onDragStop = function(event, ui) {
      //  $log.log("onDragStop event: "+event+" ui:"+ui);
    };

    vm.onResizeStart = function(event, ui) {
      //  $log.log("onResizeStart event: "+event+" ui:"+ui);
    };

    vm.onResizeStop = function(event, ui) {
      //  $log.log("onResizeStop event: "+event+" ui:"+ui);
    };

    vm.onItemAdded = function(item) {
      //  $log.log("onItemAdded item: "+item);
    };

    vm.onItemRemoved = function(item) {
      //  $log.log("onItemRemoved item: "+item);
    };
    }

})();
